const tokenService = require("../services/token-service");

const auth = async (req, res, next) => {
  try {
    let token = req.header("Authorization");
    
    
    if (!token) {
      return res.status(401).json({ msg: "Auth token access denied" });
    }

    // Check if token starts with "Bearer " and remove it
    if (token.startsWith("Bearer ")) {
      token = token.slice(7); // Remove "Bearer " from the beginning
    }

    const decodedToken = await tokenService.verifyAccessToken(token);
    
    if (!decodedToken) {
      return res
        .status(401)
        .json({ msg: "Token verification failed, authorization denied." });
    }
    
   
    next();
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal server error" });
  }
};

module.exports = auth;
