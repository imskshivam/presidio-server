const mongoose = require("mongoose");
const Message = require("../Constants/StringConstants");

const userSchema = new mongoose.Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  password: { type: String },
  email: { type: String, default: "" },

  phoneNumber: { type: String, default: "" },
  accessToken: { type: String, default: "" },
  refreshToken: { type: String, default: "" },

  createdAt: {
    type: Date, // Changed type to Date
    default: Date.now, // Automatically set to the current date and time
  },
});

const UserModel = mongoose.model("User", userSchema);
module.exports = UserModel;
