const mongoose = require("mongoose");

const propertiesSchema = new mongoose.Schema({
  userId: { type: String, default: "" },
  name: { type: String, required: true },
  description: { type: String, default: "" },
  price: { type: Number, required: true },
  images: [{ type: String, default: "" }],
  createdAt: { type: Date, default: Date.now },
  location: { type: String, default: "" },
  bedrooms: { type: Number, default: 0 },
  bathrooms: { type: Number, default: 0 },
  size: { type: Number, default: 0 },
  isAvailable: { type: Boolean, default: true },
});

const properties = mongoose.model("properties", propertiesSchema);

module.exports = properties;
