
const cloudinary = require("cloudinary").v2;


cloudinary.config({
  cloud_name: "dka9ujhvo",
  api_key: "689244497416562",
  api_secret: "ov5VjhoIU5LfuFQ-z6zpA9dED0A",
});



const uploadToCloudinary = async (file) => {
  try {
    if (!file || !file.buffer) {
      throw new Error("File buffer not found or invalid format");
    }

    let cloudinaryOptions = {
      resource_type: "auto", // Let Cloudinary determine the resource type
      folder: "Agricon-Uploads",
    };

    // Check if the file is an image or a video based on its MIME type
    if (file.mimetype.startsWith("image")) {
      cloudinaryOptions.resource_type = "image";
    } else if (file.mimetype.startsWith("video")) {
      cloudinaryOptions.resource_type = "video";
    }

    // Upload file buffer to Cloudinary using upload_stream
    const cloudinaryResult = await new Promise((resolve, reject) => {
      const cld_upload_stream = cloudinary.uploader.upload_stream(
        cloudinaryOptions,
        function (error, result) {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        }
      );

      // Pipe the file buffer to the Cloudinary upload stream
      require("stream").Readable.from(file.buffer).pipe(cld_upload_stream);
    });

    console.log("Uploaded to Cloudinary:", cloudinaryResult.secure_url);

    // Return the secure URL of the uploaded file
    return cloudinaryResult.secure_url;
  } catch (error) {
    console.error("Error uploading file to Cloudinary:", error);
    throw new Error("Failed to upload file to Cloudinary");
  }
};



const deleteFromCloudinary = async (imageUrl) => {
  try {
    // Extract the public_id from the Cloudinary URL
    const publicId = imageUrl.split("/v")[1].split("/")[1];

    // Delete the image from Cloudinary using the public_id
    const result = await cloudinary.uploader.destroy(publicId);

    // Log the result
    console.log("Deleted from Cloudinary:", result);

    // Return success message
    return "Successfully deleted from Cloudinary";
  } catch (error) {
    // Log and throw error if deletion fails
    console.error("Error deleting image from Cloudinary:", error);
    throw new Error("Failed to delete image from Cloudinary");
  }
};

module.exports = {

  uploadToCloudinary,
  deleteFromCloudinary,
};
