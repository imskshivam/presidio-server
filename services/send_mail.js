const nodemailer = require("nodemailer");
const fs = require("fs");

async function sendEmail(sendto, text, subject) {
  try {
    // Load the HTML template from the file
   

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "t4623244@gmail.com",
        pass: "yras pfsi esnq hjiq",
      },
    });

    const mailOptions = {
      from: "t4623244@gmail.com",
      to: sendto,
      subject: subject,
      text: text,
      // html: emailTemplate,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  } catch (error) {
    console.error("Error reading email template:", error);
  }
}

async function sendEmailOTP(otp, sendto, text) {
  try {
    // Load the HTML template from the file
    const emailTemplate = fs.readFileSync("services/new-email.html", "utf8");

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "t4623244@gmail.com",
        pass: "yras pfsi esnq hjiq",
      },
    });

    const mailOptions = {
      from: "t4623244@gmail.com",
      to: sendto,
      subject: otp,
      text: text,
      html: emailTemplate,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  } catch (error) {
    console.error("Error reading email template:", error);
  }
}

module.exports = {
  sendEmail,
  sendEmailOTP,
};
