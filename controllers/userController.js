const User = require("../models/UserModel"); // Assuming UserModel is exported from the specified path
const StatusCodes = require("../Constants/statusCode");
const Messages = require("../Constants/StringConstants");
const {
  formatResponse,
  formatResponseWithPagination,
} = require("../helper/responseHelper");

exports.getUserById = async (req, res) => {
  const userId = req.params.userId; // Assuming userId is provided in the request parameters

  try {
    // Fetch the user by their ID
    const user = await User.findById(userId);

    if (!user) {
      return formatResponse(
        res,
        StatusCodes.USER_NOT_FOUND,
        Messages.USER_NOT_FOUND
      );
    }

    return formatResponse(
      res,
      StatusCodes.SUCCESS,
      "user retrieved successfully",
      user
    );
  } catch (error) {
    console.error("Error retrieving properties:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};
