const otpService = require("../services/otp-services");
const hashService = require("../services/hash-service");
const tokenService = require("../services/token-service");
const User = require("../models/UserModel");
const StatusCodes = require("../Constants/statusCode");
const { formatResponse } = require("../helper/responseHelper");
const Messages = require("../Constants/StringConstants");
const bcrypt = require("bcrypt");
const { sendEmail } = require("../services/send_mail");

async function emailLogin(req, res) {
  const { email, password } = req.body;

  try {
    // Find user by email
    const user = await User.findOne({ email });

    // Check if user exists
    if (!user) {
      return formatResponse(
        res,
        StatusCodes.UNAUTHORIZED,
        "Invalid email or password"
      );
    }

    // Compare the provided password with the hashed password stored in the database
    const passwordMatch = await bcrypt.compare(password, user.password);

    // If passwords don't match, return unauthorized
    if (!passwordMatch) {
      return formatResponse(
        res,
        StatusCodes.UNAUTHORIZED,
        "Invalid email or password"
      );
    }

    // If user exists and password matches, return user data
    return formatResponse(res, StatusCodes.SUCCESS, null, user);
  } catch (error) {
    console.error("Error during email login:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error"
    );
  }
}

async function registerWithEmail(req, res) {
  const { email } = req.body;
  // Regular expression to validate email format
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  if (!email || !emailRegex.test(email)) {
    return formatResponse(res, StatusCodes.BAD_REQUEST, Messages.INVALID_EMAIL);
  }

  try {
    // Check if email already exists in the database
    const existingUser = await User.findOne({ email });

    if (existingUser) {
      return formatResponse(
        res,
        StatusCodes.BAD_REQUEST,
        "E-mail already registered"
      );
    }

    const otp = await otpService.generateOtp();

    // Set OTP expiration time (5 minutes)
    const ttl = 1000 * 60 * 5;
    const expires = Date.now() + ttl;

    // Concatenate user data to create a hash
    const data = `${email}.${otp}.${expires}`;

    // Generate hash for OTP
    const hash = hashService.hashOtp(data);

    // Send OTP via email
    await sendEmail(email, `${otp}`, "OTP");

    return formatResponse(res, StatusCodes.SUCCESS, Messages.OTP_GENERATED, {
      hash: `${hash}.${expires}`,
      otp: otp,
    });
  } catch (error) {
    console.error(error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      Messages.INTERNAL_SERVER_ERROR
    );
  }
}

async function verifyOtpforEmail(req, res) {
  const { otp, hash, email, password, phone, firstName, lastName } = req.body;
  if (!otp || !hash || !email || !phone || !firstName || !lastName) {
    return formatResponse(
      res,
      StatusCodes.BAD_REQUEST,
      Messages.ALL_FIELDS_REQUIRED
    );
  }

  const [hashedOtp, expires] = hash.split(".");

  if (Date.now() > +expires) {
    return formatResponse(res, StatusCodes.BAD_REQUEST, Messages.OTP_EXPIRED);
  }

  const data = `${email}.${otp}.${expires}`;

  let isValid = await otpService.verifyOtp(hashedOtp, data);

  if (!isValid) {
    return formatResponse(res, StatusCodes.BAD_REQUEST, Messages.INVALID_OTP);
  } else {
    // Hash the password before saving to the database
    const hashedPassword = await bcrypt.hash(password, 10);

    // Find the user by email
    const getUser = await User.findOne({ email: email });

    if (getUser != null) {
      return formatResponse(
        res,
        StatusCodes.BAD_REQUEST,
        "Email is already registered"
      );
    }

    const { accessToken, refreshToken } = tokenService.generateTokens({
      id: email,
      activate: false,
    });

    // User not found, create a new user
    const user = new User();
    user.email = email;
    user.password = hashedPassword; // Save hashed password
    user.accessToken = accessToken;
    user.phoneNumber = phone;
    user.first_name = firstName;
    user.last_name = lastName;
    const savedUser = await user.save();

    return formatResponse(res, StatusCodes.SUCCESS, Messages.USER_CREATED, {
      user: savedUser,
      isNew: true,
    });
  }
}

module.exports = {
  emailLogin,
  registerWithEmail,
  verifyOtpforEmail,
};
