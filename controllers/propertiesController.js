const Properties = require("../models/PropertiesModel");
const StatusCodes = require("../Constants/statusCode");
const {
  formatResponse,
  formatResponseWithPagination,
} = require("../helper/responseHelper");
const Messages = require("../Constants/StringConstants");
const { uploadToS3, uploadToCloudinary } = require("../services/uploadToS3");
const { sendEmail } = require("../services/send_mail");

exports.createproperties = async (req, res) => {
  try {
    const {
      name,
      description,
      price,
      userId,
      location,
      bedrooms,
      bathrooms,
      size,
      isAvailable,
    } = req.body;

    const files = req.files;

    // Check if at least one image file was uploaded
    if (!files || files.length === 0) {
      return formatResponse(
        res,
        StatusCodes.BAD_REQUEST,
        "At least one image file is required"
      );
    }

    const uploadedImageUrls = [];

    // Upload each image file to Cloudinary
    for (const file of files) {
      const imageUrl = await uploadToCloudinary(file);
      uploadedImageUrls.push(imageUrl);
    }

    // Create a new properties instance
    const newProperty = new Properties({
      name: name,
      description: description,
      price: price,
      userId: userId,
      images: uploadedImageUrls,
      location: location,
      bedrooms: bedrooms,
      bathrooms: bathrooms,
      size: size,
      isAvailable: isAvailable,
    });

    // Save the property to the database
    const savedProperty = await newProperty.save();

    // Return success response
    return formatResponse(
      res,
      StatusCodes.CREATED,
      "Property created successfully",
      savedProperty
    );
  } catch (error) {
    console.error("Error creating property:", error);

    // Return error response
    return formatResponse(
      res,
      StatusCodes.INTERNAL_SERVER_ERROR,
      "Failed to create property",
      null,
      error.message
    );
  }
};

exports.getAllproperties = async (req, res) => {
  try {
    const page = req.query.page ? parseInt(req.query.page) : 1;
    const limit = req.query.limit ? parseInt(req.query.limit) : 10;

    const count = await Properties.countDocuments();
    const totalPages = Math.ceil(count / limit);
    const currentPage = page;

    const propertiess = await Properties.find()
      .skip((currentPage - 1) * limit)
      .limit(limit);

    return formatResponseWithPagination(
      res,
      StatusCodes.SUCCESS,
      "propertiess retrieved successfully",
      propertiess,
      totalPages,
      currentPage
    );
  } catch (error) {
    console.error("Error retrieving propertiess:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};

exports.getAlluserproperties = async (req, res) => {
  try {
    const userId = req.query.id; // Assuming user ID is available in the request object after authentication

    const page = req.query.page ? parseInt(req.query.page) : 1;
    const limit = req.query.limit ? parseInt(req.query.limit) : 10;

    const count = await Properties.countDocuments({ userId }); // Filter by userId
    const totalPages = Math.ceil(count / limit);
    const currentPage = page;

    const properties = await Properties.find({ userId }) // Filter by userId
      .skip((currentPage - 1) * limit)
      .limit(limit);

    return formatResponseWithPagination(
      res,
      StatusCodes.SUCCESS,
      "Properties retrieved successfully",
      properties,
      totalPages,
      currentPage
    );
  } catch (error) {
    console.error("Error retrieving properties:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};

// Get a single properties by ID
exports.getpropertiesById = async (req, res) => {
  try {
    const properties = await Properties.findById(req.query.id);
    if (!properties) {
      return formatResponse(
        res,
        StatusCodes.NOT_FOUND,
        "properties not found",
        null
      );
    }
    return formatResponse(
      res,
      StatusCodes.SUCCESS,
      "properties retrieved successfully",
      properties
    );
  } catch (error) {
    console.error("Error retrieving properties:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};

// Update a properties by ID
exports.updateproperties = async (req, res) => {
  try {
    const updatedproperties = await Properties.findByIdAndUpdate(
      req.query.id,
      req.body
    );
    if (!updatedproperties) {
      return formatResponse(
        res,
        StatusCodes.NOT_FOUND,
        "properties not found",
        null
      );
    }
    return formatResponse(
      res,
      StatusCodes.SUCCESS,
      "properties updated successfully",
      updatedproperties
    );
  } catch (error) {
    console.error("Error updating properties:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};

exports.deleteproperties = async (req, res) => {
  try {
    const deletedproperties = await Properties.findByIdAndDelete(req.params.id);
    if (!deletedproperties) {
      return formatResponse(
        res,
        StatusCodes.NOT_FOUND,
        "properties not found",
        null
      );
    }
    return formatResponse(
      res,
      StatusCodes.SUCCESS,
      "properties deleted successfully",
      deletedproperties
    );
  } catch (error) {
    console.error("Error deleting properties:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};

exports.searchpropertiess = async (req, res) => {
  try {
    const { collectionId, categoryId, query, page, limit } = req.query;

    let filter = {};

    // Check if collectionId is provided
    if (collectionId) {
      filter.collectionId = collectionId;
    }

    // Check if categoryId is provided
    if (categoryId) {
      filter.categoryId = categoryId;
    }

    if (query) {
      // Add search condition for name or description
      filter.$or = [
        { name: { $regex: query, $options: "i" } }, // Case-insensitive search for name
      ];
    }

    // Parse page and limit parameters
    const pageNumber = parseInt(page) || 1;
    const pageSize = parseInt(limit) || 10;

    // Calculate skip value
    const skip = (pageNumber - 1) * pageSize;

    // Execute the search query to count total matching propertiess
    const totalpropertiessCount = await Properties.countDocuments(filter);

    // Calculate total pages
    const totalPages = Math.ceil(totalpropertiessCount / pageSize);

    // Execute the search query with pagination
    const propertiess = await properties
      .find(filter)
      .skip(skip)
      .limit(pageSize);

    return formatResponseWithPagination(
      res,
      StatusCodes.SUCCESS,
      "propertiess retrieved successfully",
      propertiess,
      pageNumber,
      totalPages // Include total pages in the response
    );
  } catch (error) {
    console.error("Error searching for propertiess:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};

exports.intrested = async (req, res) => {
  try {
    const {
      senderEmail,
      recieverEmail,
      name,
      phone,
      property_name,
      property_Id,
    } = req.body;

    // Send email with user's interest in the property
    await sendEmail(
      recieverEmail,
      `Name: ${name}\nPhone: ${phone}\nProperty Name: ${property_name}\npropertyId : ${property_Id}\nE-mail : ${senderEmail}`,

      `${name} Intrested in ${property_name}`
    );

    // Respond with success message
    return formatResponse(res, StatusCodes.SUCCESS, "Request sent");
  } catch (error) {
    console.error("Error searching for properties:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      "Internal server error",
      null,
      error.message
    );
  }
};
