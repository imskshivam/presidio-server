const formatResponse = require("../helper/responseHelper");
const StatusCodes = require("../Constants/statusCode");
const Messages = require("../Constants/StringConstants");
const Wishlist = require("../models/WishlistModel");

exports.addToWishlist = async (req, res) => {
  try {
    const { userId, productId } = req.body;

    // Check if the product already exists in the wishlist for the given user
    const existingWishlistItem = await Wishlist.findOne({ productId });

    // If the product already exists, return a message indicating it's already in the wishlist
    if (existingWishlistItem) {
      return formatResponse(
        res,
        StatusCodes.CONFLICT,
        "Product already exists in the wishlist"
      );
    }

    // If the product doesn't exist, add it to the wishlist
    const wishlistItem = await Wishlist.create({ userId, productId });

    return formatResponse(
      res,
      StatusCodes.CREATED,
      "Item added to wishlist successfully",
      wishlistItem
    );
  } catch (error) {
    console.error("Error adding item to wishlist:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      Messages.INTERNAL_SERVER_ERROR
    );
  }
};

exports.getWishlistItems = async (req, res) => {
  try {
    const userId = req.query.userId;
    const page = req.query.page ? parseInt(req.query.page) : 1;
    const limit = req.query.limit ? parseInt(req.query.limit) : 10;

    const wishlistItems = await Wishlist.aggregate([
      { $match: { userId } }, // Filter by userId
      { $skip: (page - 1) * limit }, // Skip documents
      { $limit: limit }, // Limit documents per page
      {
        $lookup: {
          from: "products", // Collection to join
          localField: "productId", // Field from wishlist
          foreignField: "_id", // Field from product
          as: "product", // Output array field
        },
      },
      { $unwind: "$product" }, // Flatten the product array
      { $project: { "product._id": 0 } }, // Exclude _id field from product
    ]);

    const count = await Wishlist.countDocuments({ userId });
    const totalPages = Math.ceil(count / limit);

    return formatResponseWithPagination(
      res,
      StatusCodes.SUCCESS,
      "Wishlist items retrieved successfully",
      wishlistItems,
      totalPages,
      page
    );
  } catch (error) {
    console.error("Error retrieving wishlist items:", error);
    return formatResponseWithPagination(
      res,
      StatusCodes.SERVER_ERROR,
      Messages.INTERNAL_SERVER_ERROR
    );
  }
};

exports.removeFromWishlist = async (req, res) => {
  try {
    const { userId, productId } = req.body;
    await Wishlist.deleteOne({ userId, productId });
    return formatResponse(
      res,
      StatusCodes.SUCCESS,
      "Item removed from wishlist successfully"
    );
  } catch (error) {
    console.error("Error removing item from wishlist:", error);
    return formatResponse(
      res,
      StatusCodes.SERVER_ERROR,
      Messages.INTERNAL_SERVER_ERROR
    );
  }
};
