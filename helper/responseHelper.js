const formatResponse = (res, statusCode, message, data = null) => {
  return res.status(statusCode).json({
    success: statusCode >= 200 && statusCode < 300,
    message,
    data,
  });
};

const formatResponseWithPagination = (
  res,
  statusCode,
  message,
  data = null,
  totalPages,
  currentPage
) => {
  return res.status(statusCode).json({
    success: statusCode >= 200 && statusCode < 300,
    message,
    data,
    totalPages,
    currentPage,
  });
};

module.exports = { formatResponse, formatResponseWithPagination };
