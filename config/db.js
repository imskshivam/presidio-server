require("dotenv").config();
const mongoose = require("mongoose");

async function connectDB() {


  // await mongoose
  //   .connect(process.env.MONGO_CONNECTION_URL)
  //   .then(() => {
  //     console.log("DataBas connected.");
  //   })
  //   .catch((e) => {
  //     console.log("Connection Failed");
  //   });

  const { connection } = await mongoose.connect(process.env.MONGO_CONNECTION_URL);
  console.log("DataBase connected.");


}

module.exports = connectDB;
