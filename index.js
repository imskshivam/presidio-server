// Import necessary modules
const express = require("express");
const cors = require("cors");
const connectDB = require("./config/db");

// Create an Express app
const app = express();

// Define port
const PORT = process.env.PORT || 3000;

// Middleware setup
app.use(cors({ origin: '*' }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Connect to database
connectDB();

// Import routes
const authRouter = require("./routes/auth");
const propertiesRouter = require("./routes/propertiesRoute");
const userRouter = require("./routes/userRoute");

// Define routes
app.get("/", (req, res) => {
  res.json({ msg: "This is Shivam" });
});

app.use("/auth", authRouter);
app.use("/properties", propertiesRouter); // Corrected route path
app.use("/user", userRouter); // Corrected route path

// Start the server
app.listen(PORT, "0.0.0.0", () => {
  console.log(`Server is running on port ${PORT}`);
});
