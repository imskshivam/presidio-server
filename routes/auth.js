const express = require("express");

const authController = require("../controllers/authController");

const authRouter = express.Router();

authRouter.post("/email-login", authController.emailLogin);



authRouter.post("/register-email", authController.registerWithEmail);

authRouter.post("/verify-otp-email", authController.verifyOtpforEmail);

module.exports = authRouter;
