const express = require("express");
const propertiesRouter = express.Router();
const propertiesController = require("../controllers/propertiesController");
const multer = require("multer");

const storage = multer.memoryStorage();

const upload = multer({ storage: storage });

propertiesRouter.post(
  "/create-properties",
  upload.array("file"),
  propertiesController.createproperties
);

propertiesRouter.get("/", propertiesController.getAllproperties);
propertiesRouter.get("/my-post", propertiesController.getAlluserproperties);

propertiesRouter.get("/getbyId", propertiesController.getpropertiesById);

propertiesRouter.put("/updatepost", propertiesController.updateproperties);

propertiesRouter.delete("/:id", propertiesController.deleteproperties);

propertiesRouter.get("/search", propertiesController.searchpropertiess);

propertiesRouter.post("/intrested", propertiesController.intrested);

module.exports = propertiesRouter;
